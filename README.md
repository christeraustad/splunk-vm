# Inmarsat Splunk Virtual Machine

A Splunk server you can use for testing.

## Prequisities

- [Virtualbox](https://www.virtualbox.org/)
- [Vagrant](http://vagrantup.com)
- [Git](https://www.git-scm.com/)
- wget

## Installation

```bash
$> git clone https://christeraustad@bitbucket.org/christeraustad/splunk-vm.git
$> vagrant up
```