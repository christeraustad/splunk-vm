# Splunk installation directory
SPLUNK_HOME='/opt/splunk'

# Splunk version to be installed
SPLUNK_VERSION='splunk-6.5.1-f74036626f0c-linux-2.6-amd64.deb'

# Check if Splunk is installed
if [[ ! -f $SPLUNK_HOME/etc/splunk.version ]]; then
	# Download Splunk if no .rpm in /tmp directory
	if [[ ! -f /tmp/$SPLUNK_VERSION ]]; then
		echo "Download Splunk"
		wget --no-verbose -O s/tmp/$SPLUNK_VERSION 'https://www.splunk.com/bin/splunk/DownloadActivityServlet?architecture=x86_64&platform=linux&version=6.5.1&product=splunk&filename='$SPLUNK_VERSION'&wget=true'
	fi

	# Install Splunk
	echo "Installing Splunk"
	dpkg -i /tmp/$SPLUNK_VERSION
	echo "Splunk installed to /opt/splunk"

	# Accept license on first boot
	sudo runuser -l splunk -c $SPLUNK_HOME'/bin/splunk start --accept-license'

	# Disable first login prompt
	# Source: http://answers.splunk.com/answers/778/how-can-i-change-the-admin-password-before-splunk-is-started-the-first-time.html
	sudo runuser -l splunk -c 'touch '$SPLUNK_HOME'/etc/.ui_login'

	# Source: http://answers.splunk.com/answers/9465/admin-password-on-command-line.html#answer-9469
	echo "Set admin user password to 'admin'"
	sudo runuser -l splunk -c $SPLUNK_HOME'/bin/splunk edit user admin -password admin -auth admin:changeme'

	# Restart for changes to take effect
	echo "Restart for changes to take effect"
	sudo runuser -l splunk -c $SPLUNK_HOME'/bin/splunk restart'

# Print version information if Splunk is installed
elif [[ -f $SPLUNK_HOME/etc/splunk.version ]]; then
	echo "Splunk version installed:"
	echo "#--------------------------------#"
	cat $SPLUNK_HOME/etc/splunk.version
	echo "#--------------------------------#"

	# Enable receiving from forwarders on port 9997.
	# TODO: Check if receiving is already enabled.
	# Source: http://docs.splunk.com/Documentation/Splunk/latest/Forwarding/Enableareceiver#Set_up_receiving_with_Splunk_CLI
	echo "Enabling receiving!"
	echo "#--------------------------------#"
	sudo runuser -l splunk -c $SPLUNK_HOME'/bin/splunk enable listen 9997 -auth admin:admin'
fi